/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_sdk_go

import (
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/sdk-go/v2/utils"
)

// MultiSignContractReq send online multi sign contract request to node
func (cc *ChainClient) MultiSignContractReq(payload *common.Payload, endorsers []*common.EndorsementEntry,
	timeout int64, withSyncResult bool) (*common.TxResponse, error) {
	return cc.sendContractRequest(payload, endorsers, timeout, withSyncResult)
}

// MultiSignContractVote send online multi sign vote request to node
func (cc *ChainClient) MultiSignContractVote(multiSignReqPayload *common.Payload, endorser *common.EndorsementEntry,
	isAgree bool, timeout int64, withSyncResult bool) (*common.TxResponse, error) {
	agree := syscontract.VoteStatus_AGREE
	if !isAgree {
		agree = syscontract.VoteStatus_REJECT
	}
	msvi := &syscontract.MultiSignVoteInfo{
		Vote:        agree,
		Endorsement: endorser,
	}
	msviByte, _ := msvi.Marshal()
	pairs := []*common.KeyValuePair{
		{
			Key:   syscontract.MultiVote_VOTE_INFO.String(),
			Value: msviByte,
		},
		{
			Key:   syscontract.MultiVote_TX_ID.String(),
			Value: []byte(multiSignReqPayload.TxId),
		},
	}
	payload := cc.createMultiSignVotePayload(pairs)

	return cc.sendContractRequest(payload, nil, timeout, withSyncResult)
}

// MultiSignContractTrig send online multi sign trig request to node
// this function is plugin after v2.3.1
func (cc *ChainClient) MultiSignContractTrig(multiSignReqPayload *common.Payload,
	timeout int64, limit *common.Limit, withSyncResult bool) (*common.TxResponse, error) {

	pairs := []*common.KeyValuePair{
		{
			Key:   syscontract.MultiVote_TX_ID.String(),
			Value: []byte(multiSignReqPayload.TxId),
		},
	}
	payload := cc.createMultiSignTrigPayload(pairs, limit)

	return cc.sendContractRequest(payload, nil, timeout, withSyncResult)
}

// MultiSignContractQuery query online multi sign
func (cc *ChainClient) MultiSignContractQuery(txId string) (*common.TxResponse, error) {

	pairs := []*common.KeyValuePair{
		{
			Key:   syscontract.MultiVote_TX_ID.String(),
			Value: []byte(txId),
		},
	}
	payload := cc.createMultiSignQueryPayload(pairs)

	resp, err := cc.proposalRequest(payload, nil)
	if err != nil {
		return resp, fmt.Errorf(errStringFormat, payload.TxType.String(), err.Error())
	}

	if err = utils.CheckProposalRequestResp(resp, false); err != nil {
		return nil, fmt.Errorf(errStringFormat, payload.TxType.String(), err.Error())
	}

	return resp, nil
}

// CreateMultiSignReqPayload create multi sign req payload
func (cc *ChainClient) CreateMultiSignReqPayload(pairs []*common.KeyValuePair) *common.Payload {

	payload := cc.CreatePayload("", common.TxType_INVOKE_CONTRACT, syscontract.SystemContract_MULTI_SIGN.String(),
		syscontract.MultiSignFunction_REQ.String(), pairs, defaultSeq, nil)
	return payload
}

// CreateMultiSignReqPayload create multi sign trig payload
func (cc *ChainClient) createMultiSignTrigPayload(pairs []*common.KeyValuePair, limit *common.Limit) *common.Payload {
	payload := cc.CreatePayload("", common.TxType_INVOKE_CONTRACT, syscontract.SystemContract_MULTI_SIGN.String(),
		syscontract.MultiSignFunction_TRIG.String(), pairs, defaultSeq, limit)

	return payload
}

// CreateMultiSignReqPayload create multi sign vote payload
func (cc *ChainClient) createMultiSignVotePayload(pairs []*common.KeyValuePair) *common.Payload {
	payload := cc.CreatePayload("", common.TxType_INVOKE_CONTRACT, syscontract.SystemContract_MULTI_SIGN.String(),
		syscontract.MultiSignFunction_VOTE.String(), pairs, defaultSeq, nil)

	return payload
}

// CreateMultiSignReqPayload create multi sign query payload
func (cc *ChainClient) createMultiSignQueryPayload(pairs []*common.KeyValuePair) *common.Payload {
	payload := cc.CreatePayload("", common.TxType_QUERY_CONTRACT, syscontract.SystemContract_MULTI_SIGN.String(),
		syscontract.MultiSignFunction_QUERY.String(), pairs, defaultSeq, nil)

	return payload
}
