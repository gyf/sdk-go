package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

import (
	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/chainmaker/sdk-go/v2/examples"
)

var (
	client *sdk.ChainClient
)

// init client create
func init() {
	fmt.Println("====================== create client ======================")
	//cli, err := examples.CreateChainClientWithSDKConf(sdkConfigPKUser1Path)
	cli, err := examples.CreateChainClientWithSDKConf(sdkConfigAdminPath)
	if err != nil {
		log.Fatalln(err)
	}
	client = cli

	fmt.Println("====================== getChainConfig ======================")
	chainConf, err := client.GetChainConfig()
	if err != nil {
		panic(err.Error())
	}
	nodes := chainConf.GetConsensus().GetNodes()
	for _, node := range nodes {
		fmt.Printf("%+v, %+v, %+v\n", node.GetNodeId(), node.GetOrgId(), node.GetAddress())
	}

	fmt.Println("====================== getCurrentBlockHeight ======================")
	blockHeight, err := cli.GetCurrentBlockHeight()
	if err != nil {
		panic(err)
	}
	fmt.Printf("currentBlockHeight: %d\n", blockHeight)
}

func main() {
	//fmt.Println("====================== 获取区块高度，根据区块高度获取链配置 ======================")
	//getChainConfigByBlockHeight(client, 1)

	//itineraryContract()

	//rustFactContract()

	//localFactGoContract()

	callOracleContract()
}

func callOracleContract() {
	params := []*common.KeyValuePair{
		{
			Key:   "url",
			Value: []byte("https://v0.yiketianqi.com/api?unescape=1&version=v61&appid=88684831&appsecret=OsSX6jwW"),
		},
		{
			Key:   "fetch_data_type",
			Value: []byte("json"),
		},
		{
			Key:   "fetch_data_formular",
			Value: []byte("/aqi/no2_desc"),
		},
		{
			Key:   "max_retry",
			Value: []byte("3"),
		},
		{
			Key:   "connection_timeout",
			Value: []byte("20"),
		},
		{
			Key:   "max_fetch_timeout",
			Value: []byte("20"),
		},
	}
	//  [code:0]/[msg:]/[contractResult:c72643f2230543c247e62a0f87537f02]
	//InvokeUserContract(ContractName, "query_http", params, true)

	params = []*common.KeyValuePair{
		{
			Key:   "query_hash",
			Value: []byte("c72643f2230543c247e62a0f87537f02"),
		},
	}
	//
	QueryUserContract(ContractName, "query_hash", params)
}

func localFactGoContract() {
	//fmt.Println("====================== 创建合约（同步）======================")
	//usernames := []string{"client1"}
	//_, err := CreateUserContract(common.RuntimeType_DOCKER_GO, ContractName, ContractVersion, ContractByteCodePath, true, true, usernames...)
	//if err != nil {
	//	log.Fatalln(err)
	//}

	//fmt.Println("====================== 升级合约 UpgradeUserContract ======================")
	//UpgradeUserContract(ContractName, UpgradeVersion, ContractByteCodePath, true, "admin1")

	// 存证
	params := []*common.KeyValuePair{
		{
			Key:   "key",
			Value: []byte("key1"),
		},
		{
			Key:   "value",
			Value: []byte("{\"id\":\"001\",\"time\":\"1670826667837\"}\n"),
		},
	}
	InvokeUserContract(ContractName, "save", params, true)

	// 查询世界状态
	params = []*common.KeyValuePair{
		{
			Key:   "key",
			Value: []byte("key1"),
		},
	}
	QueryUserContract(ContractName, "query", params)

	// 查询历史状态
	params = []*common.KeyValuePair{
		{
			Key:   "key",
			Value: []byte("key1"),
		},
	}
	QueryUserContract(ContractName, "queryHistory", params)
}

func factContract() {
	//fmt.Println("====================== 创建合约（同步）======================")
	//usernames := []string{"admin1"}
	//txResp, err := CreateUserContract(ContractName, ContractVersion, ContractByteCodePath, true, true, usernames...)
	//if err != nil {
	//	log.Fatalln(err)
	//}
	////time.Sleep(5 * time.Second)
	//
	//fmt.Println("====================== 根据txId查询合约 ======================")
	//tx := getUserContractByTxId(txResp.TxId)
	//fmt.Printf("result code:%d, msg:%s\n", tx.Transaction.Result.Code, tx.Transaction.Result.Code.String())
	//fmt.Printf("contract result code:%d, msg:%s\n", tx.Transaction.Result.ContractResult.Code, tx.Transaction.Result.ContractResult.Message)

	//fmt.Println("====================== 升级合约 UpgradeUserContract ======================")
	//UpgradeUserContract(ContractName, UpgradeVersion, ContractByteCodePath, true, "admin1")

	//excelFileBytes, err := os.ReadFile("/tmp/1667875213878.xlsx")
	//if err != nil {
	//	panic(err)
	//}
	//currTime := strconv.FormatInt(time.Now().Unix(), 10)
	//params := []*common.KeyValuePair{
	//	{
	//		Key:   "file",
	//		Value: excelFileBytes,
	//	},
	//	{
	//		Key:   "method",
	//		Value: []byte("readFile"),
	//	},
	//	{
	//		Key:   "time",
	//		Value: []byte(currTime),
	//	},
	//}
	//dataBytes := InvokeUserContract(ContractName, "invoke_contract", params, true)
	//fmt.Printf("data:%+v\n", string(dataBytes))

	currTime := strconv.FormatInt(time.Now().Unix(), 10)
	params := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("exportFile"),
		},
		{
			Key:   "time",
			Value: []byte(currTime),
		},
	}
	dataBytes := QueryUserContract(ContractName, "invoke_contract", params)
	fmt.Printf("%+v\n", string(dataBytes))
	err := os.WriteFile(fmt.Sprintf("/tmp/test-%d.xlsx", time.Now().UnixNano()), dataBytes, 0644)
	if err != nil {
		panic(err)
	}

}

func rustFactContract() {
	// 创建合约，行踪合约
	//res, err := CreateUserContract(common.RuntimeType_WASMER, ContractName, ContractVersion, ContractByteCodePath, true, true, "client1")
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Printf("create contract result: %s\n", res.ContractResult.String())

	// 执行合约：save
	//currTime := strconv.FormatInt(time.Now().Unix(), 10)
	//params := []*common.KeyValuePair{
	//	{
	//		Key:   "file_hash",
	//		Value: []byte("test_file_1"),
	//	},
	//	{
	//		Key:   "file_name",
	//		Value: []byte("test_file_1"),
	//	},
	//	{
	//		Key:   "time",
	//		Value: []byte(currTime),
	//	},
	//}
	//InvokeUserContract(ContractName, "save", params, true)

	// 查询行踪
	params := []*common.KeyValuePair{
		{
			Key:   "file_hash",
			Value: []byte("test_file_1"),
		},
	}
	QueryUserContract(ContractName, "find_by_file_hash", params)
	QueryUserContract(ContractName, "how_to_use_iterator", params)

}

func itineraryContract() {
	// 创建合约，行踪合约
	//res, err := CreateUserContract(common.RuntimeType_DOCKER_GO, ContractName, ContractVersion, ContractByteCodePath, true, true, "client1")
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Printf("create contract result: %s\n", res.ContractResult.String())
	//
	//fmt.Println("====================== 升级合约 UpgradeUserContract ======================")
	//UpgradeUserContract(ContractName, UpgradeVersion, ContractByteCodePath, true, "client1")

	// 执行合约：上报行踪
	//params := []*common.KeyValuePair{
	//	{
	//		Key:   "phone",
	//		Value: []byte("18891401498"),
	//	},
	//	{
	//		Key:   "itinerary",
	//		Value: []byte(GetIpInfo()),
	//	},
	//}
	//InvokeUserContract(ContractName, "save", params, true)

	start := time.Now()
	// 查询行踪
	params := []*common.KeyValuePair{
		{
			Key:   "phone",
			Value: []byte("18891401498"),
		},
	}
	QueryUserContract(ContractName, "queryHistory", params)
	fmt.Printf("QueryUserContract ContractName=[%s] time cost: %v\n", ContractName, time.Since(start))
}

func GetIpInfo() string {
	// curl https://ipinfo.io?token=8db4b68c584a81
	params := make(map[string]string, 0)
	params["token"] = "8db4b68c584a81"
	res, err := NewHttpRequest().Get("https://ipinfo.io?", nil, params)
	if err != nil {
		panic(err)
	}
	//fmt.Println(res)
	return res
}

// 获取链信息 blockHeight
func getChainConfigByBlockHeight(client *sdk.ChainClient, blockHeight uint64) {
	resp, err := client.GetChainConfigByBlockHeight(blockHeight)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("blockHeight=%d GetChainConfig resp: %+v\n", blockHeight, resp)

	currentHeight, err := client.GetCurrentBlockHeight()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("currentHeight: %d\n", currentHeight)

	archiveHeight, err := client.GetArchivedBlockHeight()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("archiveHeight: %d\n", archiveHeight)
}
