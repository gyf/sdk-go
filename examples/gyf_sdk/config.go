package main

import "fmt"

const (
	createContractTimeout = 60

	ContractVersion = "1"
	UpgradeVersion  = "2"
	//ContractName         = "factOnMacOSx64"
	ContractName = "user_contract_v2"
	//ContractByteCodePath = "/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/contracts-go/deposit/factOnMacOSx64.7z"
	ContractByteCodePath = "/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/chainmaker-oracle/standard_oracle_contract/user_contract_demo/user_contract_v2.7z"

	//ContractVersion      = "1"
	//UpgradeVersion       = "2"
	//ContractName         = "ItineraryContract05"
	//ContractByteCodePath = "/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/contracts-go/itinerary/ItineraryContract05.7z"

	//ContractVersion      = "1"
	//UpgradeVersion       = "2"
	//ContractName         = "fact"
	//ContractByteCodePath = "/Users/admin/Workspace/rust/git.chainmaker.org.cn/contracts-rust/fact/target/wasm32-unknown-unknown/release/fact.wasm"

	//chainId            = "envtest04"
	//sdkConfigOrgDomain = "www.test3.com"

	chainId            = "chain1"
	sdkConfigOrgDomain = "wx-org1.chainmaker.org"

	//chainId            = "chainmaker_testnet_chain"
	//sdkConfigOrgDomain = "org5.cmtestnet"

	UserNameOrg1Client1 = "org1client1"
	UserNameOrg2Client1 = "org2client1"

	UserNameOrg1Admin1 = "org1admin1"
	UserNameOrg2Admin1 = "org2admin1"
	UserNameOrg3Admin1 = "org3admin1"
)

type User struct {
	TlsKeyPath, TlsCrtPath   string
	SignKeyPath, SignCrtPath string
}

type PkUsers struct {
	SignKeyPath string
}

type PermissionedPkUsers struct {
	SignKeyPath string
	OrgId       string
}

var (
	sdkConfigAdminPath = "/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + ".yml"

	userConfMap = map[string]*User{
		"admin1": {
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/admin1/admin1.tls.key",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/admin1/admin1.tls.crt",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/admin1/admin1.sign.key",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/admin1/admin1.sign.crt",
		},
		"client1": {
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.tls.key",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.tls.crt",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.sign.key",
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.sign.crt",
		},
	}

	permissionedPkUsers = map[string]*PermissionedPkUsers{
		"client1": {
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.tls.key",
			sdkConfigOrgDomain,
		},
	}

	pkUsers = map[string]*PkUsers{
		"client1": {
			"/Users/admin/Workspace/golang/src/git.chainmaker.org.cn/sdk-go/examples/gyf_sdk/" + fmt.Sprintf("%s-%s", chainId, sdkConfigOrgDomain) + "/user/client1/client1.tls.key",
		},
	}
)
